<?

header('Content-Type: text/html; charset=utf-8');
include_once dirname(dirname(__FILE__)) . "/libs/libs.php";

$base_url = "http://" . $_SERVER['HTTP_HOST'] . "/workers/server";

$servers = Array($base_url . "1.php",
    $base_url . "2.php",
    $base_url . "3.php",
    $base_url . "4.php",
    $base_url . "5.php",
    $base_url . "6.php",
    $base_url . "7.php",
    $base_url . "8.php",);

$balancer = new Balancer("random.txt", $servers, $method);

if ($balancer->test_code()) {
    echo json_encode($balancer->execute());
} else {
    $balancer->out_error("Неверный код доступа. Программа самоуничтожения активирована. Асталявиста, яхуууу!");
}
?>