<?

class Server extends MasterMind {

    protected $right_commands = array("status", "math");

    function __construct($log_name = "server.txt") {
        $this->log_file_name = $log_name;
        return parent::__construct();
    }

    protected function action_status() {
        $this->log("СТАТУС: готов к действию!");
        return $this->out_data("OK");
    }

    protected function action_math() {
        if (!isset($_GET['math'])) {
            return $this->out_error("you need math. expression");
        };
        $answer = SimpleMath::calculate_and_print($_GET['math']);
        $this->log("ВЫЧИСЛЕНИЕ команды и её результат: " . $answer);
        return $this->out_data("OK", Array("out" => $answer));
    }

}

?>