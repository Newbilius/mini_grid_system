<?

/*
 * Простейший интерпретатор математических выражений. 
 * Принимает только цифры (разделение разрядов - точкой), а так же операции +,-,*,/.
 */

class SimpleMath {

    static public function filter($expression) {
        return preg_replace('/[^0-9\(\)+\.\*\/-]/is', '', $expression);
    }

    static public function calculate($expression) {
        $out = false;
        @$result = eval('$out = ' . SimpleMath::filter($expression) . ';');

        if ($out === false)
            return false;
        return $out;
    }

    static public function calculate_and_print($expression) {
        $result = SimpleMath::calculate($expression);
        if ($result === false)
            $result = "ОШИБКА ДЕЛЕНИЯ НА ОГУРЕЦ. ПОЖАЛУЙСТА, ПЕРЕЗАПУСТИТЕ ВСЕЛЕННУЮ.(c)Hex<br>А если серьезно, у вас ошибка в математическом выражении.";

        return SimpleMath::filter($expression) . "=" . $result;
    }

}

?>