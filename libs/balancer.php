<?

class Balancer extends MasterMind {

    protected $right_commands = array("math");
    protected $servers = array();
    protected $mode = "";

    function __construct($log_name = "balancer.txt", $server_array = array(), $mode = "random") {
        $this->log_file_name = $log_name;
        $this->servers = $server_array;
        $this->mode = $mode;
        return parent::__construct();
    }

    //выбираем случайный сервер. просто случайный.
    protected function select_server_random() {
        $ok = false;
        while ($ok == false) {
            $server = $this->servers[array_rand($this->servers)];
            $this->log("ПРОВЕРЯЮ сервер " . $server);
            $data = $this->get_data($server, Array(
                "code" => $this->work_code,
                "command" => "status",
                    ));
            if (isset($data)) {
                if ($data) {
                    if (is_array($data)) {
                        if (isset($data['status']))
                            if ($data['status'] == "OK") {
                                $this->log("ВЫБРАН сервер " . $server);
                                $ok = true;
                                return $server;
                            }
                    }
                }
            }
            $this->log("ОШИБКА выбора сервера " . $server);
        }
        return false;
    }

    protected function select_server() {
        $method_name = "select_server_" . $this->mode;
        return $this->$method_name();
    }

    protected function action_math() {
        $server = $this->select_server();
        $math = SimpleMath::filter($_GET['math']);
        $this->log("ПЕРЕДАЮ серверу {$server} выражение {$math}");
        return $this->get_data($server, Array(
                    "code" => $this->work_code,
                    "command" => "math",
                    "math" => $_GET['math']
                ));
        //return $this->out_data("OK", Array("out"=>$answer));
    }

}

?>