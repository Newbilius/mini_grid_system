<?

class MasterMind {

    public $errors;
    protected $work_code = "cjhjrnsczxj,tpmzxyytxnjytcnfylfhnyjtcjndjhbkbcahenrjvnfrbtltfk";
    protected $right_commands = array();
    public $log_file_name = "simple.txt";

    function __construct() {
        if (!function_exists("curl_init")) {
            return false;
        }
        return $this;
    }

    protected function log($text) {
        file_put_contents($this->log_file_name, date("d.m.Y H:i:s") . " " . $text . "\r\n", FILE_APPEND | LOCK_EX);
    }

    protected function _Curl($url) {
        $result = false;
        if (function_exists("curl_init")) {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_FAILONERROR, 1);
            curl_setopt($ch, CURLOPT_VERBOSE, 1);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_TIMEOUT, 90);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            $result = curl_exec($ch);
            if ($result == false) {
                $this->errors[] = curl_error($ch);
            }
            curl_close($ch);
        } else {
            $this->errors[] = "не подключег модуль curl.";
        }

        return $result;
    }

    public function test_code() {
        if (isset($_GET['code'])) {
            if ($_GET['code'] == $this->work_code)
                return true;
        }
        return false;
    }

    public function out_data($status, $data = array()) {
        $data['status'] = $status;
        echo json_encode($data);
    }

    public function get_data($url, $params) {
        $result = false;
        $params_url = false;
        if ((is_array($params) && $url)) {
            foreach ($params as $code => $param) {
                if (!$params_url) {
                    $params_url.="?";
                } else {
                    $params_url.="&";
                };
                $params_url.=$code . "=" . urlencode($param);
            }
            $result = $this->_Curl($url . $params_url);
            $result = json_decode($result, true);
        } else {
            $this->errors[] = "неверная ссылка или параметры.";
        }

        return $result;
    }

    protected function select_command() {
        if (isset($_GET['command']))
            if (in_array($_GET['command'], $this->right_commands))
                return $_GET['command'];
        return "index";
    }

    public function out_error($text) {
        $this->log("ОШИБКА: " . $text);
        return $this->out_data("error", Array("out" => $text));
    }

    protected function action_index() {
        return $this->out_error("Насяльника, моя твоя мало-мало понимать сапсем нету! [i don't know this command]");
    }

    public function execute() {
        $action = "action_" . $this->select_command();
        $this->log("ВЫПОЛНЯЮ КОМАНДУ: " . $action);
        return $this->$action();
    }

}

?>